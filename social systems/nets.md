nets are human-formed-and-inhabited networks. functionally, they are similar to guilds. when they get large, they begin to be targets of [[infiltrations]].

often players in a net will fill bounties together. in this case, they are matchmade with other players in a net and pitted against each other.

## destruction

when a net is destroyed by an infiltration, the [[pact hub|pact]] bond of every member is reduced to 0.