raids are monthly events where users can raid other cities controlled by the [[the old web]], because a being of "massive sentience" has been detected and must be dispatched.

these end with an incredibly difficult boss and can drop [[source hub|sources]], weapons, and cosmetics. raids can be performed with a net, with multiple nets, or alone. the less people involved, the greater the reward.