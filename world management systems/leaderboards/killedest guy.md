the killedest guy leaderboard tracks the top 100 deaths that have had the most "overkill", i.e. if damage was counted past 0 hp which death would have the least (most negative) hp. this counts even if a player has their [[stock]] remaining.

the death is recorded and can be replayed.