deliveries are time trials across the city. players are given a pickup spot and a destination, and have to make the distance in a certain amount of time. occasionally, they will be raced by another player instead of having a set amount of time.

sometimes, deliveries are actually [[dead drops]], and vice versa.