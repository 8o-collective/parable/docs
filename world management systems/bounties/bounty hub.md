#hub 

bounties are given out by organizations through agents in [[bars]]. they drop loot, almost always [[weapon hub|weapons]] and [[cosmetic hub|cosmetics]]. completing bounties often increases reputation with the host organization, but can also reduce reputation with another organization. bounties are generated through the [[overarching system]].

bounties are the main activity for players in parable.

this document tracks bounties.

- [[dead drops]]
- [[deliveries]]
- [[espionage]]