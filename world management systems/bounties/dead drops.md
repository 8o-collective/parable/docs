dead drops are flags placed in [[arenas]] that must be picked up by a player. for this kind of bounty, the player can also set it to alert them when the drop has been "placed" (in reality this is matchmaking with another opponent). the vast majority of the time, the player will encounter enemies at the arena, and leave with the flag and return to the [[bars|bar]] that they got the bounty from.

a player who is holding the flag gets [[hypervisibility]]. the flag itself is always outlined. when a player leaves the arena, they have 30 seconds before their hypervisibility wears off. in this time, enemies can still damage them.

sometimes, dead drops are actually [[deliveries]], and vice versa.