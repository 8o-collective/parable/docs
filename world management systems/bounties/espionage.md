espionage bounties are bounties given for a player to track a player in another net. this reduces the reputation of that net with the host organization. the objective of the espionage is to follow the player without being "spotted"; directly looked at by the target.

often, the target has been given a bounty themselves to complete, to make the espionage interesting.