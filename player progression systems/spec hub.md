#hub 

a player's spec is their build, consisting of their [[pact hub|pact]] on top, their [[path hub|path]] as middleware, and the [[precept hub|precept]] on the bottom. this structure relates the sunk cost of each - the pact is relatively much harder to respec than precept, and more time has to be poured into it to access all of its nuances and levels.

the player's spec also determines their [[title]], a unique descriptor that encapsulates their entire build for other players to quickly evaluate - both for combat and to admire.