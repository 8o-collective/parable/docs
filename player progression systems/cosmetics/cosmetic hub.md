#hub 

cosmetics are wearable items. 

there are several classes of cosmetics, each one containing a set of templates. cosmetics are completely randomized based loosely on this template. cosmetics can be acquired through [[bounty hub|bounties]] and [[event hub|events]], and traded at [[bazaars]]. not all cosmetics are equally probable, however. some skins and templates are more rare than others.