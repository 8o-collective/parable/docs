#hub 

paths are a naturally evolving level system tied with an achievement system that over time strengthens the players in situations that they were already likely to find themselves in. 

paths are made of two parts: a [[conditions|condition]] and a resulting [[effects|effect]]. if the player satisfies the condition, they earn the effect (typically a buff).

in order to spec into these conditions and effects, there are unlock conditions that require a number of successes in order to unlock the ability to associate a bonus with that condition. 

each condition has multiple [[tiers]] that get progressively more difficult to satisfy (and unlock). a condition and all associated tiers are referred to as that path.

in each tier, the effect becomes stronger. this is the tradeoff of paths - you can select an easy to achieve tier and earn a mild effect, or a much harder tier and earn a severe effect.
