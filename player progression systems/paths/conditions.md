
<table>
	<tr>
		<td></td>
		<th scope="col">condition</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>While airborne for 1 second…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>While airborne for 3 seconds…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>While airborne for 5 seconds…</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">condition</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>After a headshot kill (until next shot or 5 seconds pass, whichever comes first)…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>After 2 headshot kills (must be within 5 seconds of each other)...</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>After 3 headshot kills (must be within 10 seconds of each other)...</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">condition</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>While under 30% health…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>While under 20% health…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>While under 10% health…</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">condition</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>While moving faster than 60 kph…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>While moving faster than 100 kph…</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>While moving faster than 140 kph…</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">condition</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>After saving a life (healing a user who would have otherwise died within 10 seconds)...</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>After saving 2 lives (must be within 5 seconds of each other)...</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>After saving 3 lives (must be within 10 seconds of each other)...</td>
		<td>#todo</td>
	</tr>
</table>
