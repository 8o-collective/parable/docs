<table>
	<tr>
		<td></td>
		<th scope="col">effect</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>…deal 2x <a href="statuses/buffs/damage.md" class="internal-link">damage</a>.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>…deal 3x damage.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>…deal 5x damage.</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">effect</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>…move 40% <a href="statuses/buffs/speed.md" class="internal-link">faster</a>.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>…move 70% faster.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>…move 100% faster.</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">effect</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>…output 50% more <a href="status/buffs/heal.md" class="internal-link">healing</a>.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>…output 75% more healing.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>…output 100% more healing.</td>
		<td>#todo</td>
	</tr>
</table>
#issue might be hellish with the "save a life" cond

<table>
	<tr>
		<td></td>
		<th scope="col">effect</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>…reduce <a href="statuses/cooldown.md" class="internal-link">cooldowns</a> by 20%.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>…reduce cooldowns by 40%.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>…reduce cooldowns by 60%.</td>
		<td>#todo</td>
	</tr>
</table>

<table>
	<tr>
		<td></td>
		<th scope="col">effect</th>
		<th scope="col">unlock</th>
	</tr>
	<tr>
		<th scope="row">tier I</th>
		<td>…weapon self-pushback is increased 100%.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier II</th>
		<td>…weapon self-pushback is increased 1,000%.</td>
		<td>#todo</td>
	</tr>
	<tr>
		<th scope="row">tier III</th>
		<td>…weapon self-pushback is increased 10,000%.</td>
		<td>#todo</td>
	</tr>
</table>
#issue this one is funny but not NEARLY as good as the others. do you see that cooldowns one? this doesn't stand a chance