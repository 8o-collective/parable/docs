runs simulations of the future allowing for advanced planning and information gathering

achieved by bonding with [[Argo]].

## mechanic

***merge*** allows you to have retroactively predicted the outcome and teleports you to a previous state, resetting ammunition, health, cooldowns, etc.

## specializations

### continuum
extends the time of merge and allows you to hold the key down to "scrub" back in time.

### savestate
gives the merge ability two separate functions: the first press creates a save state that lasts up to 15 seconds, the second press merges into that state. automatically merges when the state expires.

### spatial decoupling
merge is replaced with a point blank area of effect which undoes damage to allies from the last 3 seconds but removes the ammunition, cooldown, and positional aspects of merge.

#issue this is way too team focused

### mastermind
tag a number of enemies with merge beacons and trigger them to send them all back in time at once. enemies don't regain health they've lost.

### fate
place traps that have a long arming time but powerful AoE temporal effects.

#issue too similar to rifle abilities? do we want multiple trap AoE stuff?