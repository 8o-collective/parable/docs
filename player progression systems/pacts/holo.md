uses bionic phosphor to generate short lived holograms designed to distract and deceive enemies

achieved by bonding with [[DIVINO]].

## mechanic

***project*** creates a replicant decoy that walks in target direction and "fires", drawing enemy attention and expiring after a short duration. if the decoy is shot at, it causes psychic damage to the aggressor that only manifests after the decoy dematerializes.

#implementation going to use a markov chain or simple rnn to make the decoy very accurate

## specializations

### chameleon
utilize hologram projection to blend into your environment. gain bonuses to stealth and access to brief [[invisibility]].

### mass projection
decrease [[cooldown]] of holograms and maintain multiple holograms at once.

### corporeality
your holograms are tangible, hard light that can interact with its environment during combat.

### dazzle
your holograms can [[stun]] and interrupt enemies. gain bonuses against stunned enemies.

### remote projection
gain the ability to control and swap places with your hologram.