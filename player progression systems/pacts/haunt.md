physically infiltrates networks and allows manifestation within network bounds at vulnerable vectors

achieved by bonding with [[Lilith]].

## mechanic

***netwalk*** into the net topology, a sort of alternate dimension where you can't be seen or interact with anything, for up to 3 seconds, then remanifest.

## specializations

### traversal
gain massive movement [[speed]] bonuses while netwalking which linger briefly upon manifestation.

### shade
gain multiple charges of netwalk and extend its duration.

### mantis
netwalk refreshes on kill and the first melee attack after netwalking is augmented.

#clarification what does it mean to augment netwalking

### shelter
netwalk's cooldown is increased. [[heal]] while in the net topology. When you netwalk, create a rift that allows allies to netwalk for a reduced duration with the same benefits.

### fusion
netwalk becomes a targeted skill. use on an enemy to enter their network and manifest at either their location or any of their nearby allies' location.