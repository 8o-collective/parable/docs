linked to a personal network of drones that can be given simple instructions and tasks

achieved by bonding with [[Simo]].

## mechanic

***release/recall*** your swarm consisting of 3 drones. when released, they can act autonomously. when destroyed, they go on cooldown before being available for release again.

## specializations

### devour
your swarm gains [[speed]], is restricted to melee, and converts corpses into additional short-lived drones.

### hunt
your swarm seeks out enemies when released and gives them [[hypervisibility]].

### trace
your swarm can be remotely controlled.

### protect
your swarm attaches to you and nearby allies, absorbing damage they would have taken.

### annihilate
your swarm seeks out and self-destructs on enemies, dealing massive damage.