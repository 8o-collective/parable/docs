#hub 

pacts are bonds made with [[source hub|sources]]. they determine access to a game-changing mechanic called the **pact mechanic**. 

the pact mechanic is a singular ability that can be modified by **specializations**.

the pact is split into multiple specializations, each of which fundamentally alter the function of the core pact mechanic, and each of which are accompanied by a skill tree for even more fine control over pact expression. pact will likely be the most game-changing and playstyle defining choice for the player.

pacts are also important for roleplaying. the source you make a pact with is an entity with its own personality and desires. the social bond with one’s source is linked to efficacy in combat. 

pacts are collectible and hidden throughout the game, though of course there are a handful available at the start. they have their own backstories and lore, and are often the result of quest lines.

this document tracks pacts.

- [[forge]]
- [[haunt]]
- [[holo]]
- [[simuspec]]
- [[swarm]]