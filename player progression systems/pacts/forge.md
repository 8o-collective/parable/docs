utilizes biohacking to generate and excrete novel chemicals and reagents

achieved by bonding with [[Dr. London]].

## mechanic

***forge*** builds meter and allows user to enter a forge state where they can consume meter to materialize consumables and power ups.

#issue i don't think we have consumables? this can be buffs ofc though

## specializations

### venom
your forge creates a variety of toxins that you apply to your weaponry, delivering [[poison]] to your enemies with each hit.

### asphyxia
your forge can release corrosive gasses which linger in an area of denial, dealing [[poison]] to enemies that pass through it.

### stimulants
your forge injects performance enhancing drugs directly into your bloodstream and releases them in the form of guided nanodarts for your allies.

### telomeratic
your forge creates repair enzymes that can [[heal]] yourself and allies over time.

### macroforge
your forge materializes a weapon that can alter to suit the environment and your needs.