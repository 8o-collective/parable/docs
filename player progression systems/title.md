each spec has a unique title formed from its components - the the title is formed as follows:

preceding adjective ([[precept hub|precept]]):
- {{precept of the breath}}: nerveless
- {{precept of the gunslinger}}: dextrous
- {{precept of the missile}}: tidal
- {{precept of the projectile}}: volatile
- {{precept of the silent}}: abyssal

prefix ([[path hub|path]] {{conditions}}):
- airborne: aero-
- headshot: neuro-
- health: retro-
- velocity: tachy-
- lifesaving: hemo-

noun ([[pact hub|pact]]):
- {{forge}}: doc
- {{haunt}}: walker
- {{holo}}: split
- {{simuspec}}: calculator
- {{swarm}}: cluster

suffix ([[path hub|path]] {{effects}}):
- 
#todo

for example, a swarm player with "While under 30% health, deal 2x damage" and precept of the projectile would have the title *Volatile Retrocluster*.

there is also an associated 4-glyph iconography representing the title as well.