#hub 

a player’s precept determines attributes of their [[weapon hub|weapon]] and skills associated with that weapon.

a single precept can apply to multiple different weapons. individual weapons are not locked to certain precepts, and vice versa.

this document tracks precepts.

- [[precept of the breath]]
- [[precept of the gunslinger]]
- [[precept of the missile]]
- [[precept of the projectile]]
- [[precept of the silent]]