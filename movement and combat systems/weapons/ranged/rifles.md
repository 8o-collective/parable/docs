low damage but high range.

1. ***mark***: Target a player and begin a countdown of 5 seconds; if within a certain range of the target for the entire duration, successfully [[marked|mark]] the target. 2 second cooldown, instant refresh if a marked enemy is killed.
2. ***trap***: Place a trap that can be remotely triggered. On an unmarked enemy, mark them. On a marked enemy, immobilize them for 10 seconds. 60 second cooldown.