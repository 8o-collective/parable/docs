slower swing speed, but higher damage.

1. ***strike***: Hit the ground to quickly pop nearby enemies into the air. 10 second cooldown.
2. ***execute***: Charge up an uncancellable attack for 3 seconds, dealing 50% more [[damage]]. Damage modifiers affect this attack 5x. 10 second cooldown.