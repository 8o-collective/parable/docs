fast swing speed, but relatively low damage.

#issue this is the same as precept of the missile
1. ***dash***: Quickly dash in any direction. This can be done mid-air, and momentum is conserved. 2 second cooldown.
2. ***barrage***: For one second, speed up swing speed by 10x. All attacks do 20% [[damage]]. 10 second cooldown.