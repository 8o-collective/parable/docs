applies {{buff hub|buffs}} and {{debuff hub|debuff}} like heals, bleeding, and knockback.

1. ***scratch***: Apply [[bleed|bleeding]] that does 0.1x damage per second for 10 seconds. 5 second cooldown.
2. ***massage***: [[heal]] teammates with a soothing backrub. 10 second cooldown.