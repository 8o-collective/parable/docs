#hub 

weapons are the primary method by which damage is done to enemies. any damage that one player does to another is considered damage done by their weapon, and hence inherits the same [[buff hub|buffs]] and [[debuff hub|debuffs]] as it.

like, [[cosmetic hub|cosmetics]], weapons unlocked through [[bounty hub|bounties]] or [[event hub|events]], and can be traded at [[bazaars]]. while each weapon class has various templates which have different stats, they are alike to cosmetics in that the visual appearance is randomized from the template model.

each weapon has two abilities, a primary and a secondary.

this document tracks weapons.

- [[bladed]]
- [[blunt]]
- [[fisticuffs]]
- [[handguns]]
- [[rifles]]
