damage increases the amount of damage a weapon does by a percentage or ratio.

when a player has damage, they will glow - this glow is relative to the amount of damage, with a higher value resulting in a brighter glow. if a player has 2x damage, streaks of lightning will begin to arc off of their person and they will make a faint tesla coil sound that gets louder as the damage proportion increases.

this glow (and sound) are only observed by players that are targets of this damage; for example, a player might have 3x damage on a specific opponent, but not on their ally. only the opponent against whom the 3x damage buff is active will see the glow, their ally will not.