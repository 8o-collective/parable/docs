#hub 

there are various debuffs that a player can experience. these debuffs decrease their power and control.

this document tracks debuffs.

- [[bleed]]
- [[hypervisibility]]
- [[marked]]
- [[poison]]
- [[stun]]