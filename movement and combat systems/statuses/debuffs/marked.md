marked targets take 10x damage. the first shot that doesn’t hit a marked enemy unmarks all enemies.

while being marked, both the attacker and the target can see distance to the other, and remaining countdown. if the target is successfully marked, they are made aware of this.

marked targets are also [[hypervisibility|hypervisible]] to the attacker

#issue is this too op? or is it the point? actually, i was thinking you could balance some weapons around being marked but not hypervisible