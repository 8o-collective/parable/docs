arenas are areas in which players deathmatch. typically they contain [[dead drops]].

when a player enters an arena, their name, status, and [[nets|net]] affiliation is broadcast to all other players in the arena.

when a player is reduced to 0 hp in an arena, they lose all [[buff hub|buffs]] and [[debuff hub|debuffs]] and begin healing all of their hp in a process that takes 15 seconds. they drop any [[dead drops|dead drop]] they might have been carrying. their [[stock]] has been depleted, and this will no longer occur if they are reduced to 0 hp. instead they will respawn at a [[reconstruction points|reconstruction point]].