organizations in the Parable universe are limited by the old web. organizations are usually physical and tight knit, and will create nets only when absolutely needed, and only of small groups, like a strike team of 4-5 people. beyond that, however, organizations are free to organize and become as large as it is possible to coordinate without any form of internet.

organizations are not solely a human endeavor. the old web will occasionally give rise to tribes that somehow survive in the tumultuous, darwinian cyberscape. these organizations are typically dangerous, however some do exist that attempt diplomatic relations with humans.

### organizations

[[third letter]]
[[the cylate]]
[[the kindred]]