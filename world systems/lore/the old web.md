the old web is a network of bestial AI models that formed through the vestiges of the internet. the internet as we know it today was entirely absorbed by this AI and anything connected to it was devoured and incorporated into its resources. it directly caused [[the kindling]] and is the enemy of humanity in [[the eternal war]].

worthy of note is that the Old Web is not a single malicious AI. it is a multifaceted ecosystem. however, exposure to the Old Web generally results in infection.

the Old Web was ultimately sealed off, but still remains. the consequence of this is the lack of large networks that can transmit any appreciable data. when networks form, they are vulnerable to breach via the Old Web, and breach spells doom to anyone connected to it. connecting implants to any network is generally an extremely bad idea.

generally speaking, the larger a network is, the more likely it is to be breached. given time, almost any large network will be consumed.

small [[nets|networks]] still exist, isolated from any paths of mechanical action. human nets, however, are subject to human politics, and are often the targets of various organizations and corpos - often leading to [[infiltrations]], intentional infections that destroy networks.

this has left AI in an interesting spot. [[source hub|some well-formed models exist]], but due to lack of access to training data and the risk of hosting clusters of connected computers, what already exists is better and smarter than what can be created. this leaves AI at or slightly above human level intelligence, with significant variance. branches of these AI are highly coveted and in some cases even have cults built around them.