the kindling refers to the beginning of [[the eternal war]], when [[the old web]] formed and began to ravage and destroy humanity where it erupted. 

just pre-kindling, humans had just begun experimenting with neurocybernetics. humans unlucky enough to have these implants connected to the internet were hacked and converted / mutated into appendages of the Old Web, and used as vectors to attack the lucky unconnected.

it is unknown how long ago the kindling occurred, but it is implied that it is somewhere between 120-140 years ago.