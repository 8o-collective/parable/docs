#hub 

sources are combat AI coprocessors, cybernetically installed into your brain. bonds with them for a symbiotic relationship are called [[pact hub|pacts]], and are highly implicated in gameplay.

sources have their own unique personalities, and are associated with specific pacts.

this document tracks sources.

- [[Argo]]
- [[DIVINO]]
- [[Dr. London]]
- [[Lilith]]
- [[Simo]]